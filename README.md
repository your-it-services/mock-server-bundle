# MockServerBundle

The aim of Mock Server Bundle is to make mocking remote API calls easier during both automated testing and local
development work, removing the need for remote API mocking code in the application itself, and ensuring that the API
calls are made outside the system under test.

This bundle makes use of [MockServer](https://www.mock-server.com/) to provide the remote mock API
(via a Docker container) and adds Symfony commands to load and reset mock API data into this system.

The mocks themselves can currently use the following formats:
- JSON
- [JSON:API](https://jsonapi.org/)
- SOAP

along with the following methods:
- GET
- DELETE
- PATCH
- POST

The bundle takes the provided mock files (see below) and assembles them into a suitable request for 
MockServer, allowing developers or testers to simply add the expected request and response payloads (for example, by 
copying existing payloads from actual API calls), without having to write code to implement them.

## Installation

The bundle uses a contrib recipe, so you will either need to enable that during install, or add the option to your
composer config with:

```console
composer config extra.symfony.allow-contrib true
```
Install the package with:

```console
composer require youritservices/mock-server-bundle --dev
```

## Enable the bundle

If you're *not* using Symfony Flex, you'll need to enable the bundle by adding the following line in the 
`config/bundles.php` file of your project:

```php
<?php

declare(strict_types=1);

return [
    ...
    YourITServices\MockServerBundle\MockServerBundle::class => ['dev' => true, 'test' => true],
    ...
];
```

## Configuration

Edit the `config/packages/mock_server.yaml` file, setting the values as appropriate for your environment. The file and
environment variable `MOCK_SERVER_HOST` will have been added to your `.env` file by the flex recipe.

```yaml
# config/packages/mock_server.yaml
when@test: &test
  mock_server:
    host_url: '%env(MOCK_SERVER_HOST)%'
    mocks_directory: '%kernel.project_dir%/tests/apiMocks'

when@dev: *test
```

### Parameters:

#### `host_url`

_Required_

The URL for the MockServer Docker container set up in your `docker-compose.yml`

#### `mocks_directory`

_Required_

The path to the root of the API mocks you want to load.

## MockServer Service

The commands make use of MockServer, so this needs to be available to your project, with the easiest being via a service
defined in `docker-compose.yml`.

```yaml
# docker-compose.yml
version: '3.9'
services:
  mock-api:
    image: mockserver/mockserver:5.15.0
    ports:
      - "1080:1080"
    environment:
      JVM_OPTIONS: -Dmockserver.enableCORSForAllResponses=true
```

MockServer provides a dashboard, so you can view the loaded mocks, as well as the requested made against the service.
If you defined the service as above, this can be accessed via http://mock-api:1080/mockserver/dashboard

## Usage

### API mocks

The API mocks can be stored in any directory structure under [mocks_directory](#mocksdirectory), allowing you to
organize your mocks how you see fit.

Each mock consists of a number of files, depending on both the format involved and the HTTP verb used, as detailed
below.

The mocks are loaded in directory order, and each directory is ordered by the sequence number of the file(s) contained
in that directory. MockServer processes the loaded mocks in the order they are created, so default or fallback responses
should be sequenced after more specific responses.

#### URL

The URL file is used for a JSON or JSON:API based mock and follows the file naming convention:

`<sequence number>.<description>.url.txt`

The file contains the following contents:

`<status code> <verb> <url encoded path> <format> <response time (optional)>`

#### SOAP

The soap file is used for a SOAP based mock and follows the file naming convention:

`<sequence number>.<description>.soap.txt`

The file contains the following contents:

`<status code> <verb> <url encoded path> <SOAP action> <response time (optional)>`

#### Request

This file contains the request body (where required) in the expected format (json or xml), exactly as it would be sent
to the remote API and follows the file naming convention:

`<sequence number>.<description>.request.<format>`

#### Response

This file contains the response body in the expected format (json or xml), exactly as it would be sent by the remote API
in response to the request, and follows the file naming convention:

`<sequence number>.<description>.response.<format>`

### Examples

##### GET (JSON)

1.sending-postcode-with-available-addresses.url.txt

```text
200 GET /v1/autocomplete/addresses/paf_1/gbr?api_key=TestAddressApiKey JSON 500ms
```

1.sending-postcode-with-available-addresses.response.json

```json
{
  "result": [
    {
      "line_1": "1 Test Street",
      "line_2": "Test Line 2",
      "line_3": "Test Line 3",
      "post_town": "TOWN",
      "traditional_county": "Test County",
      "postal_county": "Test County",
      "postcode": "POST C01",
      "id": "paf_1"
    }
  ],
  "code": 2000,
  "message": "Success",
  "limit": 100,
  "page": 0,
  "total": 1
}
```

#### POST (JSON:API)

1.create-customer.url.txt

```text
201 POST /api/v1/test JSON:API 100ms
```

1.create-customer.request.json

```json
{
  "data": {
    "type": "customers",
    "attributes": {
      "firstName": "John",
      "lastName": "Doe",
      "email": "john.doe@example.com"
    }
  }
}
```

1.create-customer.response.json

```json
{
  "jsonapi": {
    "version": "1.0"
  },
  "meta": {
    "version": "0.1.0"
  },
  "data": {
    "type": "customer",
    "id": "1",
    "attributes": []
  }
}
```

#### POST (SOAP)

1.validate-address.soap.txt

```text
200 POST /services/ValidateAddressServiceSoap ValidateAddressRequest 100ms
```

1.validate-address.request.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<soap-env:envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="https://xmlns.example.com/Address/ValidateAddress">
    <soap-env:header></soap-env:header>
    <soap-env:body>
        <ns1:ValidateAddressRequest>
        </ns1:ValidateAddressRequest>
    </soap-env:body>
</soap-env:envelope>
```

1.validate-address.response.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<soap-env:envelope xmlns:soap-env="http://www.w3.org/2003/05/soap-envelope">
    <soap-env:body>
        <ns0:ValidateAddressResponse xmlns:ns0="https://xmlns.example.com/Address/ValidateAddress">
        </ns0:ValidateAddressResponse>
    </soap-env:body>
</soap-env:envelope>
```

### Commands

#### Load mocks

This loads all the available mocks in the configured [mocks_directory](#mocksdirectory)

```console
bin/console mockserver:load
```

#### Reset / remove mocks

This resets / removes all the currently loaded mocks, normally used before a load to ensure only the current
mocks are loaded / available.

```console
bin/console mockserver:reset
```

## Development

The bundle comes with a Makefile to allow for an easy development environment setup.  Simply clone the repository,
ensure the requirements are met, then run `make up`

### Requirements

- [Docker (Desktop or CLI) v20.10 CE or higher](https://www.docker.com/products/docker-desktop)
- [Docker Compose v1.28 or higher](https://docs.docker.com/compose/install/)

### Basic Commands

Start all Docker containers
```console
make up
```

Stop all Docker containers
```console
make down
```

Clean slate (remove vendor and rebuild containers)
```console
make rebuild
```

Run all tests before pushing to remote branch and triggering CI pipeline
```console
make pre-push
```

All available make commands
```console
make help
```
