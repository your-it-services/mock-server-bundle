SHELL=/bin/bash

-include .env.build
-include .env.build.local
-include .env
-include .env.local
-include .env.test
-include .env.test.local
export

LOCAL_USER_ID:=$(shell id -u)
LOCAL_GROUP_ID:=$(shell id -g)
DEBUG_CONTAINER_ENABLED?="true"
COMPOSE:=docker-compose -f docker/docker-compose.yml
DEV:=dev
DEBUG:=debug
COVERAGE:=coverage
RUN_AS_ROOT:=$(COMPOSE) exec $(DEV)
RUN_AS_USER:=$(COMPOSE) exec --user $(LOCAL_USER_ID) $(DEV)
RUN_AS_ROOT_DEBUG:=$(COMPOSE) exec $(DEBUG)
RUN_AS_USER_DEBUG:=$(COMPOSE) exec --user $(LOCAL_USER_ID) $(DEBUG)
COVERAGE_MINIMUM_PERCENTAGE:=75

help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

up: ## Setup development container and dependencies
	@if ! [[ -e .env.local ]]; then \
  		echo -e "# Add local environment overrides here" > .env.local; \
  	fi
	@if ! [[ -e .env.test.local ]]; then \
  		echo -e "# Add local test environment overrides here" > .env.test.local; \
  	fi
	@if ! [[ -e .env.build.local ]]; then \
  		echo -e "# Add local build environment overrides here\n" > .env.build.local; \
  	fi
	@$(COMPOSE) pull $(COVERAGE)
	@if [[ "$(DEBUG_CONTAINER_ENABLED)" == "true" ]]; then \
		$(COMPOSE) up --build --detach $(DEBUG) $(DEV) > /dev/null; \
	else \
		$(COMPOSE) up --build --detach $(DEV) > /dev/null; \
	fi
	@$(COMPOSE) up --detach $(COVERAGE)
	@mkdir -p build/logs/php
	@mkdir -p build/php/coverage
	@$(MAKE) install-dependencies

down: ## Stop Docker containers
	@$(COMPOSE) down

rebuild: ## Remove all containers / images and rebuild
	@$(MAKE) nuke
	@$(MAKE) up

nuke: ## Remove all Docker containers / images
	@if ! [[ -e .env.local ]]; then \
  		echo -e "# Add local environment overrides here\n" > .env.local; \
  	fi
	@if ! [[ -e .env.test.local ]]; then \
  		echo -e "# Add local test environment overrides here\n" > .env.test.local; \
  	fi
	@if ! [[ -e .env.build.local ]]; then \
  		echo -e "# Add local build environment overrides here\n" > .env.build.local; \
  	fi
	@$(COMPOSE) down --rmi all --volumes
	@rm -rf vendor
	@rm -f composer.lock

install-dependencies: ## Install front and backend dependencies
	@$(RUN_AS_USER) composer install --no-scripts

shell: ## Shell for running commands on the container
	@$(RUN_AS_USER) bash || true

root-shell: ## Shell for running root commands on the container
	@$(RUN_AS_ROOT) bash || true

composer-update: ## Update existing composer packages; pass parameter package="package(s)" for individual package(s)
	@if [[ -z "$(package)" ]]; then \
		$(RUN_AS_USER) composer update; \
	else \
		$(RUN_AS_USER) composer update $(package); \
	fi

composer-require: ## Add new composer packages; pass parameter package="package(s)", dev=true|false
	@if [[ -z "$(package)" ]]; then \
		echo "make composer-require requires the package argument to be provided"; \
	elif [[ "$(dev)" == "true" ]]; then \
		$(RUN_AS_USER) composer require --dev $(package); \
	else \
		$(RUN_AS_USER) composer require $(package); \
	fi

lint-php: ## Linting for Symfony / PSR12 standards; pass fix=true to automatically fix issues
	@if [[ -n "$(fix)" ]]; then \
		$(RUN_AS_USER) vendor/bin/phpcbf --standard=phpcs.xml.dist -p; \
		$(RUN_AS_USER) vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php --diff; \
	else \
		$(RUN_AS_USER) vendor/bin/phpcs --standard=phpcs.xml.dist -p; \
		$(RUN_AS_USER) vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php --dry-run --diff; \
	fi

test-phpstan: ## Run PHPStan
	@$(RUN_AS_USER) vendor/bin/phpstan analyse --memory-limit=512M

test-php-unit: ## Run PHPUnit unit test suite; pass filter= to run specific test(s)
	@if [[ -z "$(filter)" ]]; then \
		$(RUN_AS_USER) vendor/bin/simple-phpunit --configuration phpunit.xml.dist --testsuite Unit; \
	else \
		$(RUN_AS_USER) vendor/bin/simple-phpunit --configuration phpunit.xml.dist --testsuite Unit --filter=$(filter); \
	fi

test-php-coverage: ## Run PHPUnit unit tests with code coverage
	@$(COMPOSE) up --detach $(DEBUG) > /dev/null
	@$(RUN_AS_ROOT_DEBUG) mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini-disabled
	@$(RUN_AS_USER_DEBUG) vendor/bin/simple-phpunit -d memory_limit=2G \
	    --configuration phpunit.xml.dist \
	    --testsuite Unit \
	    --coverage-text \
	    --coverage-clover build/logs/php/coverage.xml \
	    --coverage-html build/php/coverage
	@$(RUN_AS_USER_DEBUG) vendor/bin/coverage-check build/logs/php/coverage.xml $(COVERAGE_MINIMUM_PERCENTAGE)
	@$(RUN_AS_ROOT_DEBUG) mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini-disabled /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

test-security: ## Run local PHP security check
	@$(RUN_AS_USER) curl -fsSL 'https://github.com/fabpot/local-php-security-checker/releases/download/v2.0.6/local-php-security-checker_2.0.6_linux_amd64' -o local-php-security-checker
	@$(RUN_AS_USER) chmod +x local-php-security-checker
	@$(RUN_AS_USER) ./local-php-security-checker

pre-commit: ## Run all linting and tests before committing PHP code
	@$(MAKE) lint-php test-php-unit test-phpstan test-security

pre-push: ## Run all linting and tests before pushing to remote branch
	@$(MAKE) lint-php test-php-coverage test-phpstan test-security
