<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('node_modules')
    ->exclude('var')
    ->exclude('vendor');

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'concat_space' => false,
        'phpdoc_line_span' => [
            'const' => 'single',
            'property' => 'single',
            'method' => 'single',
        ],
        'types_spaces' => false,
        'no_leading_import_slash' => true,
        'single_line_throw' => false,
        'class_definition' => [
            'space_before_parenthesis' => true,
        ],
        'global_namespace_import' => [
            'import_classes' => true,
        ],
    ])
    ->setFinder($finder)
    ->setUsingCache(false)
    ->setHideProgress(false);
