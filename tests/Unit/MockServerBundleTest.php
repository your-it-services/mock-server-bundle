<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Tests\Unit;

use PHPUnit\Framework\TestCase;
use YourITServices\MockServerBundle\DependencyInjection\MockServerExtension;
use YourITServices\MockServerBundle\MockServerBundle;

class MockServerBundleTest extends TestCase
{
    public function testGetContainerExtension(): void
    {
        $sut = new MockServerBundle();
        $this->assertInstanceOf(MockServerExtension::class, $sut->getContainerExtension());
    }
}
