<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Tests\Unit\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use YourITServices\MockServerBundle\Command\ResetMocksCommand;
use YourITServices\MockServerBundle\Exception\MockServerException;
use YourITServices\MockServerBundle\Service\MockServerService;

class ResetMocksCommandTest extends TestCase
{
    public function testConfigure(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $sut = new ResetMocksCommand($mockServerServiceMock);
        $this->assertSame('mockserver:reset', $sut->getName());
        $this->assertSame('Reset / clear the API mocks in MockServer', $sut->getDescription());
    }

    public function testExecuteLoadsMocksSuccessful(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $mockServerServiceMock->expects($this->once())
            ->method('resetMocks');
        $inputInterfaceMock = $this->createMock(InputInterface::class);
        $outputInterfaceMock = $this->createMock(OutputInterface::class);
        $outputInterfaceMock->expects($this->once())
            ->method('writeln');
        $sut = new ResetMocksCommand($mockServerServiceMock);
        $this->assertSame(0, $sut->run($inputInterfaceMock, $outputInterfaceMock));
    }

    public function testExecuteLoadsMocksUnsuccessful(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $mockServerServiceMock->expects($this->once())
            ->method('resetMocks')
            ->willThrowException(new MockServerException());
        $inputInterfaceMock = $this->createMock(InputInterface::class);
        $outputInterfaceMock = $this->createMock(OutputInterface::class);
        $outputInterfaceMock->expects($this->once())
            ->method('writeln');
        $sut = new ResetMocksCommand($mockServerServiceMock);
        $this->assertSame(1, $sut->run($inputInterfaceMock, $outputInterfaceMock));
    }
}
