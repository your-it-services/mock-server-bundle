<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Tests\Unit\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use YourITServices\MockServerBundle\Command\LoadMocksCommand;
use YourITServices\MockServerBundle\Exception\MockServerException;
use YourITServices\MockServerBundle\Service\MockServerService;

class LoadMocksCommandTest extends TestCase
{
    public function testConfigure(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $sut = new LoadMocksCommand($mockServerServiceMock);
        $this->assertSame('mockserver:load', $sut->getName());
        $this->assertSame('Load the API mocks into MockServer', $sut->getDescription());
    }

    public function testExecuteLoadsMocksSuccessful(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $mockServerServiceMock->expects($this->once())
            ->method('loadMocks');
        $inputInterfaceMock = $this->createMock(InputInterface::class);
        $outputInterfaceMock = $this->createMock(OutputInterface::class);
        $outputInterfaceMock->expects($this->once())
            ->method('writeln');
        $sut = new LoadMocksCommand($mockServerServiceMock);
        $this->assertSame(0, $sut->run($inputInterfaceMock, $outputInterfaceMock));
    }

    public function testExecuteLoadsMocksUnsuccessful(): void
    {
        $mockServerServiceMock = $this->createMock(MockServerService::class);
        $mockServerServiceMock->expects($this->once())
            ->method('loadMocks')
            ->willThrowException(new MockServerException());
        $inputInterfaceMock = $this->createMock(InputInterface::class);
        $outputInterfaceMock = $this->createMock(OutputInterface::class);
        $outputInterfaceMock->expects($this->once())
            ->method('writeln');
        $sut = new LoadMocksCommand($mockServerServiceMock);
        $this->assertSame(1, $sut->run($inputInterfaceMock, $outputInterfaceMock));
    }
}
