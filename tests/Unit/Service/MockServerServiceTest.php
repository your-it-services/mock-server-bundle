<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Tests\Unit\Service;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use YourITServices\MockServerBundle\Exception\MockServerException;
use YourITServices\MockServerBundle\Service\MockServerService;

class MockServerServiceTest extends TestCase
{
    private const JSON_API_CONTENT = ['content-type' => ['application/vnd.api+json']];
    private const JSON_API_ACCEPT = ['accept' => ['application/vnd.api+json']];
    private const JSON_CONTENT = ['content-type' => ['application/json']];
    private const JSON_ACCEPT = ['accept' => ['application/json']];
    private const XML = ['content-type' => ['application/xml']];

    private MockServerService $sut;
    private MockObject $httpClient;

    private vfsStreamDirectory $filesystemRoot;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        $structure = [
            'tests' => [],
            'nested' => [
                'tests' => [],
            ],
        ];
        $this->filesystemRoot = vfsStream::setup('apiMocksTests', null, $structure);
        $this->sut = new MockServerService($this->httpClient, 'http://mock-api:1080', $this->filesystemRoot->url());
    }

    public function testResetMocksWithMockServerAvailable(): void
    {
        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);
        $this->httpClient->expects($this->once())
            ->method('request')
            ->with('PUT', 'http://mock-api:1080/mockserver/reset')
            ->willReturn($mockResponse);

        $this->sut->resetMocks();
    }

    public function testResetMocksWithMockServerUnavailable(): void
    {
        $this->expectException(MockServerException::class);
        $this->expectExceptionMessage('API mocking failed: MockServer unavailable');
        $this->httpClient->expects($this->once())
            ->method('request')
            ->with('PUT', 'http://mock-api:1080/mockserver/reset')
            ->willThrowException(new TransportException('Network Errors'));

        $this->sut->resetMocks();
    }

    /** @dataProvider providerExpectations */
    public function testLoadMocksWithMockServerUnavailable(array $data): void
    {
        $this->httpClient->expects($this->once())
            ->method('request')
            ->with('PUT', 'http://mock-api:1080/expectation')
            ->willThrowException(new TransportException('Network Errors'));

        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::SERVER_UNAVAILABLE);
    }

    /** @dataProvider providerExpectations */
    public function testLoadMocksWithMockServerAvailable(array $data): void
    {
        $expected = $this->createMockServerExpectation(
            $data['urlOrSoapFileContents'],
            $data['statusCode'],
            $data['method'],
            $data['path'],
            $data['soapAction'] ?? null,
            $data['responseTime'],
            $data['request'] ?? null,
            $data['response'] ?? null,
            $data['filter'] ?? null,
            $data['requestHeaders'],
            $data['responseHeaders'],
            $data['createFile']
        );

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(201);
        $this->httpClient->expects($this->once())
            ->method('request')
            ->with('PUT', 'http://mock-api:1080/expectation', $expected)
            ->willReturn($mockResponse);

        $this->sut->loadMocks();
    }

    public function providerExpectations(): array
    {
        return [
            'Create POST JSON:API mock' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'request' => $this->getJsonApiRequestWithData(),
                    'urlOrSoapFileContents' => '201 POST /api/v1/test JSON:API 100ms',
                    'statusCode' => 201,
                    'method' => 'POST',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_API_CONTENT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create PATCH JSON:API mock' => [
                [
                    'response' => $this->getJsonApiResponseOnlyMetaData(),
                    'request' => $this->getJsonApiRequestWithData(),
                    'urlOrSoapFileContents' => '200 PATCH /api/v1/test/99999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'PATCH',
                    'path' => '/api/v1/test/99999',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_API_CONTENT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create DELETE JSON:API mock' => [
                [
                    'response' => $this->getJsonApiResponseOnlyMetaData(),
                    'urlOrSoapFileContents' => '200 DELETE /api/v1/test/99999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'DELETE',
                    'path' => '/api/v1/test/99999',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create DELETE JSON:API mock with no response data' => [
                [
                    'urlOrSoapFileContents' => '204 DELETE /api/v1/test/99999 JSON:API 100ms',
                    'statusCode' => 204,
                    'method' => 'DELETE',
                    'path' => '/api/v1/test/99999',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create GET JSON:API mock' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create POST JSON mock' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequest(),
                    'urlOrSoapFileContents' => '201 POST /api/v1/test JSON 100ms',
                    'statusCode' => 201,
                    'method' => 'POST',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_CONTENT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create PATCH JSON mock' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequest(),
                    'urlOrSoapFileContents' => '200 PATCH /api/v1/test/99999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'PATCH',
                    'path' => '/api/v1/test/99999',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_CONTENT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create DELETE JSON mock' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 DELETE /api/v1/test/9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'DELETE',
                    'path' => '/api/v1/test/9999',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create GET JSON mock' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
            'Create POST SOAP mock' => [
                [
                    'response' => $this->getSoapResponse(),
                    'request' => $this->getSoapRequest(),
                    'urlOrSoapFileContents' => '200 POST /services/ValidateAddressServiceSoap ValidateAddressRequest 100ms',
                    'statusCode' => 200,
                    'method' => 'POST',
                    'path' => '/services/ValidateAddressServiceSoap',
                    'responseTime' => 100,
                    'soapAction' => 'ValidateAddressRequest',
                    'requestHeaders' => self::XML,
                    'responseHeaders' => self::XML,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerMissingFileExpectations */
    public function testLoadMocksWithMissingUrlOrSoapFile(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::MISSING_URL_OR_SOAP_FILE);
    }

    public function providerMissingFileExpectations(): array
    {
        return [
            'SOAP file missing' => [
                [
                    'response' => $this->getSoapResponse(),
                    'request' => $this->getSoapRequest(),
                    'urlOrSoapFileContents' => '200 POST /services/ValidateAddressServiceSoap ValidateAccountRequest 100ms',
                    'statusCode' => 200,
                    'method' => 'POST',
                    'path' => '/services/ValidateAddressServiceSoap',
                    'responseTime' => 100,
                    'soapAction' => 'ValidateAccountRequest',
                    'requestHeaders' => self::XML,
                    'responseHeaders' => self::XML,
                    'createFile' => false,
                ],
            ],
            'JSON:API URL file missing' => [
                [
                    'response' => $this->getJsonApiResponseOnlyMetaData(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => false,
                ],
            ],
            'JSON URL file missing' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => false,
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidUrlPathExpectations */
    public function testLoadMocksWithInvalidUrlPath(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::INVALID_PATH);
    }

    public function providerInvalidUrlPathExpectations(): array
    {
        return [
            'SOAP file missing with invalid path' => [
                [
                    'response' => $this->getSoapResponse(),
                    'request' => $this->getSoapRequest(),
                    'urlOrSoapFileContents' => '200 POST http:///services/ValidateAddressServiceSoap ValidateAccountRequest 100ms',
                    'statusCode' => 200,
                    'method' => 'POST',
                    'path' => '/services/ValidateAddressServiceSoap',
                    'responseTime' => 100,
                    'soapAction' => 'ValidateAccountRequest',
                    'requestHeaders' => self::XML,
                    'responseHeaders' => self::XML,
                    'createFile' => true,
                ],
            ],
            'JSON:API URL file with invalid path' => [
                [
                    'response' => $this->getJsonApiResponseOnlyMetaData(),
                    'urlOrSoapFileContents' => '200 GET http:///api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'JSON URL file with invalid path' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 GET http:///api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidMethodForRequestWithBodyExpectations */
    public function testLoadMocksWithInvalidMethodForRequestWithBody(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException(
            $data,
            sprintf(MockServerException::REQUEST_CREATION_FAILED_FOR_TYPE, $data['method'])
        );
    }

    public function providerInvalidMethodForRequestWithBodyExpectations(): array
    {
        return [
            'GET JSON:API mock with request body' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'request' => $this->getJsonApiRequestWithData(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'GET JSON mock with request body' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequest(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidMethodForRequestExpectations */
    public function testLoadMocksWithInvalidMethodForRequest(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::INVALID_METHOD);
    }

    public function providerInvalidMethodForRequestExpectations(): array
    {
        return [
            'JSON:API mock with invalid method' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'request' => $this->getJsonApiRequestWithData(),
                    'urlOrSoapFileContents' => '200 INVALID /api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => 'INVALID',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'JSON mock with invalid method' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequest(),
                    'urlOrSoapFileContents' => '200 INVALID /api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => 'INVALID',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerMissingMethodExpectations */
    public function testLoadMocksWithMissingMethod(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::INVALID_METHOD);
    }

    public function providerMissingMethodExpectations(): array
    {
        return [
            'JSON:API mock with missing method' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'request' => $this->getJsonApiRequestWithData(),
                    'urlOrSoapFileContents' => '200 /api/v1/test?parameter=9999 JSON:API 100ms',
                    'statusCode' => 200,
                    'method' => '',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'JSON mock with missing method' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequest(),
                    'urlOrSoapFileContents' => '200 /api/v1/test?parameter=9999 JSON 100ms',
                    'statusCode' => 200,
                    'method' => '',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidResponseTimeExpectations */
    public function testLoadMocksWithInvalidResponseTime(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException($data, MockServerException::INVALID_RESPONSE_TIME);
    }

    public function providerInvalidResponseTimeExpectations(): array
    {
        return [
            'SOAP file missing with invalid response time' => [
                [
                    'response' => $this->getSoapResponse(),
                    'request' => $this->getSoapRequest(),
                    'urlOrSoapFileContents' => '200 POST /services/ValidateAddressServiceSoap ValidateAccountRequest 100',
                    'statusCode' => 200,
                    'method' => 'POST',
                    'path' => '/services/ValidateAddressServiceSoap',
                    'responseTime' => 100,
                    'soapAction' => 'ValidateAccountRequest',
                    'requestHeaders' => self::XML,
                    'responseHeaders' => self::XML,
                    'createFile' => true,
                ],
            ],
            'JSON:API URL file with invalid response time' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API 100',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'JSON URL file with invalid response time' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON 100',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerDefaultResponseTimeExpectations */
    public function testLoadMocksWithDefaultResponseTime(array $data): void
    {
        $expected = $this->createMockServerExpectation(
            $data['urlOrSoapFileContents'],
            $data['statusCode'],
            $data['method'],
            $data['path'],
            $data['soapAction'] ?? null,
            $data['responseTime'],
            $data['request'] ?? null,
            $data['response'] ?? null,
            $data['filter'] ?? null,
            $data['requestHeaders'],
            $data['responseHeaders'],
            $data['createFile']
        );

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(201);
        $this->httpClient->expects($this->exactly(1))
            ->method('request')
            ->with('PUT', 'http://mock-api:1080/expectation', $expected)
            ->willReturn($mockResponse);

        $this->sut->loadMocks();
    }

    public function providerDefaultResponseTimeExpectations(): array
    {
        return [
            'SOAP file with no response time provided' => [
                [
                    'response' => $this->getSoapResponse(),
                    'request' => $this->getSoapRequest(),
                    'urlOrSoapFileContents' => '200 POST /services/ValidateAddressServiceSoap ValidateAccountRequest',
                    'statusCode' => 200,
                    'method' => 'POST',
                    'path' => '/services/ValidateAddressServiceSoap',
                    'responseTime' => 0,
                    'soapAction' => 'ValidateAccountRequest',
                    'requestHeaders' => self::XML,
                    'responseHeaders' => self::XML,
                    'createFile' => true,
                ],
            ],
            'JSON:API URL file with no response time provided' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 0,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                ],
            ],
            'JSON URL file with no response time provided' => [
                [
                    'response' => $this->getJsonResponse(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 0,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidJsonDataExpectations */
    public function testLoadMocksWithInvalidJsonData(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException(
            $data,
            sprintf(MockServerException::INVALID_JSON_DATA, $data['fileName'])
        );
    }

    public function providerInvalidJsonDataExpectations(): array
    {
        return [
            'JSON:API with invalid JSON request data' => [
                [
                    'response' => $this->getJsonApiResponseWithNoData(),
                    'request' => [
                        'content' => '{',
                        'format' => 'json',
                    ],
                    'urlOrSoapFileContents' => '201 POST /api/v1/test JSON:API 100ms',
                    'statusCode' => 201,
                    'method' => 'POST',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_API_CONTENT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                    'fileName' => '1.mock-file-for-testing.request.json',
                ],
            ],
            'JSON:API with invalid JSON response data' => [
                [
                    'response' => [
                        'content' => '{',
                        'format' => 'json',
                    ],
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON:API',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_API_ACCEPT,
                    'responseHeaders' => self::JSON_API_CONTENT,
                    'createFile' => true,
                    'fileName' => '1.mock-file-for-testing.response.json',
                ],
            ],
            'JSON with invalid JSON request data' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => [
                        'content' => '{',
                        'format' => 'json',
                    ],
                    'urlOrSoapFileContents' => '201 POST /api/v1/test JSON 100ms',
                    'statusCode' => 201,
                    'method' => 'POST',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_CONTENT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                    'fileName' => '1.mock-file-for-testing.request.json',
                ],
            ],
            'JSON with invalid JSON response data' => [
                [
                    'response' => [
                        'content' => '{',
                        'format' => 'json',
                    ],
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 JSON',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                    'fileName' => '1.mock-file-for-testing.response.json',
                ],
            ],
        ];
    }

    /** @dataProvider providerInvalidRequestFormatExpectations */
    public function testLoadMocksWithInvalidRequestFormat(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException(
            $data,
            sprintf($data['exception'], $data['request']['format'])
        );
    }

    /** @dataProvider providerInvalidResponseFormatExpectations */
    public function testLoadMocksWithInvalidResponseFormat(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException(
            $data,
            sprintf($data['exception'], $data['response']['format'])
        );
    }

    public function providerInvalidRequestFormatExpectations(): array
    {
        return [
            'Mock with invalid request format' => [
                [
                    'response' => $this->getJsonResponse(),
                    'request' => $this->getJsonRequestWithInvalidFormat(),
                    'urlOrSoapFileContents' => '201 POST /api/v1/test INVALID 100ms',
                    'statusCode' => 201,
                    'method' => 'POST',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'requestHeaders' => self::JSON_CONTENT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                    'exception' => MockServerException::REQUEST_CREATION_FAILED_FOR_FORMAT,
                ],
            ],
        ];
    }

    public function providerInvalidResponseFormatExpectations(): array
    {
        return [
            'Mock with invalid response format' => [
                [
                    'response' => $this->getJsonResponseWithInvalidFormat(),
                    'urlOrSoapFileContents' => '200 GET /api/v1/test?parameter=9999 INVALID 100ms',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                    'exception' => MockServerException::RESPONSE_CREATION_FAILED_FOR_FORMAT,
                ],
            ],
        ];
    }

    /** @dataProvider providerEmptyFileExpectations */
    public function testLoadMocksWithEmptyFile(array $data): void
    {
        $this->runLoadMocksAndCheckForExpectedException(
            $data,
            sprintf(MockServerException::EMPTY_MOCK_FILE, $data['fileName'])
        );
    }

    public function providerEmptyFileExpectations(): array
    {
        return [
            'Mock with empty file' => [
                [
                    'urlOrSoapFileContents' => '',
                    'statusCode' => 200,
                    'method' => 'GET',
                    'path' => '/api/v1/test',
                    'responseTime' => 100,
                    'filter' => [
                        'parameter' => ['9999'],
                    ],
                    'requestHeaders' => self::JSON_ACCEPT,
                    'responseHeaders' => self::JSON_CONTENT,
                    'createFile' => true,
                    'fileName' => '1.mock-file-for-testing.url.txt',
                ],
            ],
        ];
    }

    private function createMockServerExpectation(
        string $urlOrSoapFileContents,
        int $statusCode,
        string $method,
        string $path,
        ?string $soapAction,
        int $responseTime,
        ?array $request,
        ?array $response,
        ?array $filter,
        array $requestHeaders,
        array $responseHeaders,
        bool $createFile
    ): array {
        $format = $request['format'] ?? $response['format'] ?? null;
        if ('xml' === $format) {
            $this->createVfsMockFiles($urlOrSoapFileContents, $request, $response, true, $createFile);
            $httpRequest = [
                'method' => $method,
                'path' => $path,
                'headers' => [
                    'content-type' => ['application/xml'],
                    'SOAPAction' => [$soapAction],
                ],
            ];
            if (!empty($request['content'])) {
                $httpRequest['body'] = [
                    'type' => 'XML',
                    'xml' => $request['content'],
                ];
            }
            $httpResponse = [
                'delay' => [
                    'timeUnit' => 'MILLISECONDS',
                    'value' => $responseTime,
                ],
                'headers' => [
                    'content-type' => ['application/xml'],
                ],
                'statusCode' => $statusCode,
            ];
            if (!empty($response['content'])) {
                $httpResponse['body'] = $response['content'];
            }
        } else {
            $this->createVfsMockFiles($urlOrSoapFileContents, $request, $response, false, $createFile);
            $httpRequest = [
                'method' => $method,
                'path' => $path,
                'headers' => $requestHeaders,
            ];
            if (!empty($request['content'])) {
                $httpRequest['body'] = [
                    'type' => 'JSON',
                    'json' => $request['content'],
                    'matchType' => 'STRICT',
                ];
            }
            if (null !== $filter) {
                $httpRequest['queryStringParameters'] = $filter;
            }
            $httpResponse = [
                'delay' => [
                    'timeUnit' => 'MILLISECONDS',
                    'value' => $responseTime,
                ],
                'headers' => $responseHeaders,
                'statusCode' => $statusCode,
            ];
            if (!empty($response['content'])) {
                $httpResponse['body'] = $response['content'];
            }
        }

        return [
            'body' => json_encode([
                'httpRequest' => $httpRequest,
                'httpResponse' => $httpResponse,
            ]),
            'headers' => [
                'content-type' => 'application/json',
            ],
        ];
    }

    private function createVfsMockFiles(
        string $urlOrSoapFileContents,
        ?array $request,
        ?array $response,
        bool $soap,
        bool $createUrlFile
    ): void {
        $description = 'mock-file-for-testing';
        if (null !== $request) {
            vfsStream::newFile('1.' . $description . '.request.' . strtolower($request['format']))
                ->withContent($request['content'])
                ->at($this->filesystemRoot->getChild('tests'));
        }
        if (null !== $response) {
            vfsStream::newFile('1.' . $description . '.response.' . strtolower($response['format']))
                ->withContent($response['content'])
                ->at($this->filesystemRoot->getChild('tests'));
        }
        if ($createUrlFile) {
            if ($soap) {
                $fileSuffix = '.soap.txt';
            } else {
                $fileSuffix = '.url.txt';
            }
            vfsStream::newFile('1.' . $description . $fileSuffix)
                ->withContent($urlOrSoapFileContents)
                ->at($this->filesystemRoot->getChild('tests'));
        }
    }

    private function runLoadMocksAndCheckForExpectedException(array $data, string $exceptionMessage): void
    {
        $this->createMockServerExpectation(
            $data['urlOrSoapFileContents'],
            $data['statusCode'],
            $data['method'],
            $data['path'],
            $data['soapAction'] ?? null,
            $data['responseTime'],
            $data['request'] ?? null,
            $data['response'] ?? null,
            $data['filter'] ?? null,
            $data['requestHeaders'],
            $data['responseHeaders'],
            $data['createFile']
        );

        $this->expectException(MockServerException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->sut->loadMocks();
    }

    private function getJsonApiResponseWithNoData(): array
    {
        return [
            'content' => json_encode([
                'jsonapi' => [
                    'version' => '1.0',
                ],
                'meta' => [
                    'version' => '0.1.0',
                ],
                'data' => [],
            ]),
            'format' => 'json',
        ];
    }

    private function getJsonApiResponseOnlyMetaData(): array
    {
        return [
            'content' => json_encode([
                'jsonapi' => [
                    'version' => '1.0',
                ],
                'meta' => [
                    'version' => '0.1.0',
                ],
            ]),
            'format' => 'json',
        ];
    }

    private function getJsonApiRequestWithData(): array
    {
        return [
            'content' => json_encode([
                'data' => [
                    'type' => '*** type ***',
                    'attributes' => [
                        'attribute1' => '*** test value 1 ***',
                        'attribute2' => '*** test value 2 ***',
                    ],
                ],
            ]),
            'format' => 'json',
        ];
    }

    private function getJsonResponse(): array
    {
        return [
            'content' => json_encode([
                'field1' => [
                    'attribute1' => '*** test value 1 ***',
                    'attribute2' => '*** test value 2 ***',
                ],
            ]),
            'format' => 'json',
        ];
    }

    private function getJsonResponseWithInvalidFormat(): array
    {
        return [
            'content' => json_encode([
                'field1' => [
                    'attribute1' => '*** test value 1 ***',
                    'attribute2' => '*** test value 2 ***',
                ],
            ]),
            'format' => 'invalid',
        ];
    }

    private function getJsonRequest(): array
    {
        return [
            'content' => json_encode([
                'field1' => [
                    'attribute1' => '*** test value 1 ***',
                    'attribute2' => '*** test value 2 ***',
                ],
            ]),
            'format' => 'json',
        ];
    }

    private function getJsonRequestWithInvalidFormat(): array
    {
        return [
            'content' => json_encode([
                'field1' => [
                    'attribute1' => '*** test value 1 ***',
                    'attribute2' => '*** test value 2 ***',
                ],
            ]),
            'format' => 'invalid',
        ];
    }

    private function getSoapResponse(): array
    {
        return [
            'content' => '<?xml version="1.0" encoding="UTF-8"?>
                <soap-env:envelope xmlns:soap-env="http://www.w3.org/2003/05/soap-envelope">
                    <soap-env:body>
                        <ns0:ValidateAddressResponse xmlns:ns0="https://xmlns.example.com/Address/ValidateAddress">
                        </ns0:ValidateAddressResponse>
                    </soap-env:body>
                </soap-env:envelope>',
            'format' => 'xml',
        ];
    }

    private function getSoapRequest(): array
    {
        return [
            'content' => '<?xml version="1.0" encoding="UTF-8"?>
                <soap-env:envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="https://xmlns.example.com/Address/ValidateAddress">
                    <soap-env:header></soap-env:header>
                    <soap-env:body>
                        <ns1:ValidateAddressRequest>
                        </ns1:ValidateAddressRequest>
                    </soap-env:body>
                </soap-env:envelope>',
            'format' => 'xml',
        ];
    }
}
