<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Tests\Unit\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use YourITServices\MockServerBundle\DependencyInjection\MockServerExtension;

class MockServerExtensionTest extends TestCase
{
    public function testDefault(): void
    {
        $container = new ContainerBuilder();
        $loader = new MockServerExtension();
        $loader->load(
            [
                [
                    'host_url' => '',
                    'mocks_directory' => '',
                ],
            ],
            $container
        );
        $loader->prepend($container);
        $this->assertTrue(
            $container->hasDefinition('your_it_services.mock_server.mock_server_service'),
            'The server service is loaded'
        );
        $this->assertTrue(
            $container->hasDefinition('your_it_services.mock_server.load_mocks_command'),
            'The load mocks command is loaded'
        );
        $this->assertTrue(
            $container->hasDefinition('your_it_services.mock_server.reset_mocks_command'),
            'The reset mocks command is loaded'
        );
        $this->assertArrayHasKey('http_client', $container->getExtensionConfig('framework')[0]);
    }
}
