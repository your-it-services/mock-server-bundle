<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class MockServerExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');
        /** @var Configuration $configuration */
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        $definition = $container->getDefinition('your_it_services.mock_server.mock_server_service');
        $definition->setArgument(1, $config['host_url']);
        $definition->setArgument(2, $config['mocks_directory']);
        $container->getDefinition('your_it_services.mock_server.load_mocks_command');
        $container->getDefinition('your_it_services.mock_server.reset_mocks_command');
    }

    public function prepend(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('framework', ['http_client' => []]);
    }
}
