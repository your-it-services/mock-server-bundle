<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('mock_server');
        $rootNode = $treeBuilder->getRootNode();
        $rootNode->children()
            ->scalarNode('host_url')
                ->isRequired()
                ->info('URL for the MockServer')
                ->example('http://mock-api:1080')
                ->end()
            ->scalarNode('mocks_directory')
                ->isRequired()
                ->info('Directory containing API mocks to load')
                ->example('tests/Fixtures/apiMocks')
                ->end();

        return $treeBuilder;
    }
}
