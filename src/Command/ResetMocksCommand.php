<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use YourITServices\MockServerBundle\Exception\MockServerException;
use YourITServices\MockServerBundle\Service\MockServerService;

class ResetMocksCommand extends Command
{
    private MockServerService $mockServerService;

    public function __construct(MockServerService $mockServerService, string $name = null)
    {
        $this->mockServerService = $mockServerService;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName('mockserver:reset')
            ->setDescription('Reset / clear the API mocks in MockServer');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->mockServerService->resetMocks();
            $output->writeln('<info>[notice] API mocks reset successfully</info>');

            return 0;
        } catch (MockServerException $exception) {
            $output->writeln(sprintf('<error>%s</error>', $exception->getMessage()));

            return 1;
        }
    }
}
