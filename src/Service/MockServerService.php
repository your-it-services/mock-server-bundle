<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Service;

use Carbon\CarbonInterval;
use Carbon\Exceptions\InvalidIntervalException;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use YourITServices\MockServerBundle\DTO\Mock;
use YourITServices\MockServerBundle\Exception\MockServerException;

class MockServerService
{
    private HttpClientInterface $httpClient;
    private string $hostUrl;
    private string $mocksDirectory;
    private string $filename;
    private string $fileContents;

    public function __construct(HttpClientInterface $httpClient, string $hostUrl, string $mocksDirectory)
    {
        $this->httpClient = $httpClient;
        $this->hostUrl = $hostUrl;
        $this->mocksDirectory = $mocksDirectory;
    }

    public function resetMocks(): void
    {
        try {
            if (200 !== $this->httpClient->request('PUT', $this->hostUrl . '/mockserver/reset')->getStatusCode()) {
                throw new MockServerException(MockServerException::RESET_FAILED);
            }
        } catch (TransportExceptionInterface $exception) {
            throw new MockServerException(MockServerException::SERVER_UNAVAILABLE);
        }
    }

    public function loadMocks(): void
    {
        $mockFileLists = $this->getMockFileLists();
        foreach ($mockFileLists as $fileList) {
            foreach ($fileList as $mock) {
                $expectation = $this->createExpectationFromMock($mock);
                $this->setExpectation($expectation);
            }
        }
    }

    private function getMockFileLists(): array
    {
        $fileLists = $this->buildFileLists();
        $mockFileLists = [];
        foreach ($fileLists as $fileList) {
            $mocks = [];
            foreach ($fileList as $file) {
                $this->filename = $file->getFilename();
                $this->fileContents = $this->getMockFileContents($file);
                [0 => $sequence, 2 => $type, 3 => $format] = explode('.', $this->filename);
                $mocks[$sequence] = $mocks[$sequence] ?? new Mock();
                switch ($type) {
                    case 'url':
                        $mocks[$sequence] = $this->createUrlMock($mocks[$sequence]);
                        break;
                    case 'soap':
                        $mocks[$sequence] = $this->createSoapMock($mocks[$sequence]);
                        break;
                    case 'request':
                        $mocks[$sequence] = $this->createRequestMock($mocks[$sequence], $format);
                        break;
                    case 'response':
                        $mocks[$sequence] = $this->createResponseMock($mocks[$sequence], $format);
                        break;
                    default:
                        throw new MockServerException(
                            sprintf(MockServerException::INVALID_MOCK_FILE, $this->filename)
                        );
                }
            }

            $mockFileLists[] = $mocks;
        }

        return $mockFileLists;
    }

    private function createExpectationFromMock(Mock $mock): array
    {
        $expectation = [
            'httpRequest' => [
                'method' => $mock->getMethod(),
                'path' => $mock->getPath(),
                'headers' => $mock->getRequest()->getHeaders(),
            ],
            'httpResponse' => [
                'delay' => [
                    'timeUnit' => 'MILLISECONDS',
                    'value' => $mock->getResponse()->getResponseTime(),
                ],
                'headers' => $mock->getResponse()->getHeaders(),
                'statusCode' => $mock->getStatusCode(),
            ],
        ];
        if (null !== $mock->getQueryString()) {
            $expectation['httpRequest']['queryStringParameters'] = $mock->getQueryString();
        }
        if (!empty($mock->getRequest()->getBody())) {
            $expectation['httpRequest']['body'] = $mock->getRequest()->getBody();
        }
        if (!empty($mock->getResponse()->getBody())) {
            $expectation['httpResponse']['body'] = $mock->getResponse()->getBody()[0];
        }
        if (null !== $mock->getSoapAction()) {
            $expectation['httpRequest']['headers']['SOAPAction'] = [$mock->getSoapAction()];
        }

        return $expectation;
    }

    private function setExpectation(array $expectation): void
    {
        try {
            $response = $this->httpClient->request('PUT', $this->hostUrl . '/expectation', [
                'body' => json_encode($expectation),
                'headers' => [
                    'content-type' => 'application/json',
                ],
            ]);
            if (Response::HTTP_CREATED !== $response->getStatusCode()) {
                throw new MockServerException(MockServerException::EXPECTATION_FAILED);
            }
        } catch (TransportExceptionInterface $exception) {
            throw new MockServerException(MockServerException::SERVER_UNAVAILABLE);
        }
    }

    private function createUrlMock(Mock $mock): Mock
    {
        $mockUrlRequest = explode(' ', $this->fileContents);
        $statusCode = $mockUrlRequest[0];
        $path = $mockUrlRequest[2];
        $jsonType = $mockUrlRequest[3];
        $mock->setStatusCode((int) $statusCode);
        $mock = $this->setUrlPath($mock, $path);
        $mock = $this->setQueryString($mock, $path);
        $mock->setMethod($this->getRequestMethodFromData());
        $mock->getResponse()->setResponseTime($this->getResponseTimeFromData());

        return $this->setRequestAndResponseHeadersNew($mock, $jsonType);
    }

    private function setUrlPath(Mock $mock, string $path): Mock
    {
        $urlPath = parse_url($path, PHP_URL_PATH);
        if (!is_string($urlPath)) {
            throw new MockServerException(MockServerException::INVALID_PATH);
        }
        $mock->setUrlPath($urlPath);

        return $mock;
    }

    private function setQueryString(Mock $mock, string $path): Mock
    {
        $queryString = parse_url($path, PHP_URL_QUERY);
        if ($queryString) {
            $queryStringArray = [];
            parse_str($queryString, $queryStringArray);
            array_walk($queryStringArray, static function (&$value): void {
                $value = [$value];
            });
            $mock->setQueryString($queryStringArray);
        }

        return $mock;
    }

    private function createSoapMock(Mock $mock): Mock
    {
        $mockUrlRequest = explode(' ', $this->fileContents);
        $statusCode = $mockUrlRequest[0];
        $path = $mockUrlRequest[2];
        $soapAction = $mockUrlRequest[3];
        $mock->setStatusCode((int) $statusCode);
        $mock = $this->setUrlPath($mock, $path);
        $mock = $this->setQueryString($mock, $path);
        $mock->setSoapAction($soapAction);
        $mock->setMethod($this->getRequestMethodFromData());
        $mock->getResponse()->setResponseTime($this->getResponseTimeFromData());
        $mock->getRequest()->addHeader(['content-type' => ['application/xml']]);
        $mock->getResponse()->addHeader(['content-type' => ['application/xml']]);

        return $mock;
    }

    private function createRequestMock(Mock $mock, string $format): Mock
    {
        $this->guardAgainstMissingMethod($mock);
        switch ($format) {
            case 'json':
                if ('PATCH' !== $mock->getMethod() && 'POST' !== $mock->getMethod()) {
                    throw new MockServerException(
                        sprintf(MockServerException::REQUEST_CREATION_FAILED_FOR_TYPE, $mock->getMethod())
                    );
                }
                $body = [
                    'type' => 'JSON',
                    'json' => json_encode($this->getJsonData()),
                    'matchType' => 'STRICT',
                ];
                break;
            case 'xml':
                $request = $this->fileContents;
                $body = [
                    'type' => 'XML',
                    'xml' => $request,
                ];
                break;
            default:
                throw new MockServerException(
                    sprintf(MockServerException::REQUEST_CREATION_FAILED_FOR_FORMAT, $format)
                );
        }

        $mock->getRequest()->setBody($body);

        return $mock;
    }

    private function createResponseMock(Mock $mock, string $format): Mock
    {
        $this->guardAgainstMissingMethod($mock);
        switch ($format) {
            case 'json':
                $responseData = $this->getJsonData();
                $mock->getResponse()->setBody([json_encode($responseData)]);

                break;
            case 'xml':
                $mock->getResponse()->setBody([$this->fileContents]);

                break;
            default:
                throw new MockServerException(
                    sprintf(MockServerException::RESPONSE_CREATION_FAILED_FOR_FORMAT, $format)
                );
        }

        return $mock;
    }

    private function getJsonData(): array
    {
        $jsonData = json_decode($this->fileContents, true);
        if (!is_array($jsonData)) {
            throw new MockServerException(sprintf(MockServerException::INVALID_JSON_DATA, $this->filename));
        }

        return $jsonData;
    }

    private function getMockFileContents(SplFileInfo $file): string
    {
        $fileContents = file_get_contents($file->getPathname());
        if (empty($fileContents)) {
            throw new MockServerException(sprintf(MockServerException::EMPTY_MOCK_FILE, $this->filename));
        }

        return trim($fileContents);
    }

    private function buildFileLists(): array
    {
        $recursiveDirectoryIterator = new RecursiveDirectoryIterator(
            $this->mocksDirectory,
            FilesystemIterator::SKIP_DOTS
        );
        $recursiveIteratorIterator = new RecursiveIteratorIterator(
            $recursiveDirectoryIterator,
            RecursiveIteratorIterator::CHILD_FIRST
        );
        $directoryList = [];
        /** @var SplFileInfo $item */
        foreach ($recursiveIteratorIterator as $item) {
            if ($item->isDir()) {
                $directoryList[] = $item;
            }
        }
        $fileLists = [];
        foreach ($directoryList as $directory) {
            $files = iterator_to_array(new FilesystemIterator($directory->getPathname()));
            /** @var SplFileInfo $file */
            foreach ($files as $index => $file) {
                if ($file->isDir()) {
                    unset($files[$index]);
                }
            }
            usort($files, [$this, 'orderFilesetForUrlOrSoapFileFirst']);
            if (!empty($files)) {
                $fileLists[] = $files;
            }
        }

        return $fileLists;
    }

    private function orderFilesetForUrlOrSoapFileFirst(SplFileInfo $file1, SplFileInfo $file2): int
    {
        [0 => $file1Sequence, 2 => $file1Type] = explode('.', $file1->getFilename());
        [0 => $file2Sequence, 2 => $file2Type] = explode('.', $file2->getFilename());
        if ($file1Sequence === $file2Sequence) {
            if ($file1Type === $file2Type) {
                return 0;
            }

            return ($file1Type > $file2Type) ? -1 : 1;
        }

        return ($file1Sequence < $file2Sequence) ? -1 : 1;
    }

    private function setRequestAndResponseHeadersNew(Mock $mock, string $jsonType): Mock
    {
        switch ($mock->getMethod()) {
            case 'GET':
            case 'DELETE':
                $requestHeaderField = 'accept';
                break;
            default:
                $requestHeaderField = 'content-type';
                break;
        }
        if ('JSON:API' === $jsonType) {
            $contentType = ['application/vnd.api+json'];
        } else {
            $contentType = ['application/json'];
        }
        $mock->getRequest()->addHeader([$requestHeaderField => $contentType]);
        $mock->getResponse()->addHeader(['content-type' => $contentType]);

        return $mock;
    }

    private function getRequestMethodFromData(): string
    {
        $urlOrSoapFileContents = preg_split('/\s/', $this->fileContents);
        if (
            is_array($urlOrSoapFileContents)
            && count($urlOrSoapFileContents) > 1
            && in_array($urlOrSoapFileContents[1], ['GET', 'DELETE', 'POST', 'PATCH'])
        ) {
            return $urlOrSoapFileContents[1];
        }
        throw new MockServerException(MockServerException::INVALID_METHOD);
    }

    private function getResponseTimeFromData(): int
    {
        $urlOrSoapFileContents = preg_split('/\s/', $this->fileContents);
        if (is_array($urlOrSoapFileContents) && count($urlOrSoapFileContents) > 4) {
            try {
                $responseTime = CarbonInterval::fromString($urlOrSoapFileContents[4]);

                return $responseTime->milliseconds;
            } catch (InvalidIntervalException $exception) {
                throw new MockServerException(MockServerException::INVALID_RESPONSE_TIME);
            }
        }

        return 0;
    }

    private function guardAgainstMissingMethod(Mock $mock): void
    {
        if (null === $mock->getMethod()) {
            throw new MockServerException(MockServerException::MISSING_URL_OR_SOAP_FILE);
        }
    }
}
