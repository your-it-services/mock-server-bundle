<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\Exception;

use Exception;

class MockServerException extends Exception
{
    public const RESET_FAILED = 'API mock expectation reset failed';
    public const EXPECTATION_FAILED = 'API mock expectation setting failed';
    public const SERVER_UNAVAILABLE = 'API mocking failed: MockServer unavailable';
    public const REQUEST_CREATION_FAILED_FOR_FORMAT = 'Request expectation creation failed for format %s';
    public const REQUEST_CREATION_FAILED_FOR_TYPE = 'Request expectation creation failed for type %s';
    public const RESPONSE_CREATION_FAILED_FOR_FORMAT = 'Response expectation creation failed for format %s';
    public const INVALID_JSON_DATA = 'Invalid JSON request or response data supplied: %s';
    public const EMPTY_MOCK_FILE = 'Empty mock file supplied: %s';
    public const INVALID_MOCK_FILE = 'Invalid mock file: %s';
    public const INVALID_METHOD = 'Invalid method in url or soap file';
    public const INVALID_RESPONSE_TIME = 'Invalid response time expectation in url or soap file';
    public const MISSING_URL_OR_SOAP_FILE = 'Method not set: url or soap file missing?';
    public const INVALID_PATH = 'Invalid path in url or soap file';
}
