<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\DTO;

class MockRequest
{
    /** @var array<string, string> */
    private array $headers = [];
    /** @var array<string, string>|null */
    private ?array $body = null;

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function addHeader(array $header): void
    {
        $this->headers += $header;
    }

    public function getBody(): ?array
    {
        return $this->body;
    }

    public function setBody(array $body): void
    {
        $this->body = $body;
    }
}
