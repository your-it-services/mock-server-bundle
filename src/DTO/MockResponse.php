<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\DTO;

class MockResponse
{
    /** @var array<string, string> */
    private array $headers = [];
    /** @var array<string, string> */
    private array $body = [];
    private int $responseTime;

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function addHeader(array $header): self
    {
        $this->headers += $header;

        return $this;
    }

    public function getBody(): array
    {
        return $this->body;
    }

    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    public function setResponseTime(int $responseTime): void
    {
        $this->responseTime = $responseTime;
    }

    public function getResponseTime(): int
    {
        return $this->responseTime;
    }
}
