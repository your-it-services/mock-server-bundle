<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle\DTO;

class Mock
{
    private MockRequest $request;
    private MockResponse $response;
    private ?string $method = null;
    /** @var array<int, string>|null */
    private ?array $queryString = null;
    private ?string $path = null;
    private int $statusCode;
    private ?string $soapAction = null;

    public function __construct()
    {
        $this->request = new MockRequest();
        $this->response = new MockResponse();
    }

    public function getRequest(): MockRequest
    {
        return $this->request;
    }

    public function getResponse(): MockResponse
    {
        return $this->response;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function getQueryString(): ?array
    {
        return $this->queryString;
    }

    public function setQueryString(array $queryString): void
    {
        $this->queryString = $queryString;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setUrlPath(string $path): void
    {
        $this->path = $path;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function getSoapAction(): ?string
    {
        return $this->soapAction;
    }

    public function setSoapAction(string $soapAction): void
    {
        $this->soapAction = $soapAction;
    }
}
