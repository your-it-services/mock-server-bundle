<?php

declare(strict_types=1);

namespace YourITServices\MockServerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use YourITServices\MockServerBundle\DependencyInjection\MockServerExtension;

class MockServerBundle extends Bundle
{
    public function getContainerExtension(): MockServerExtension
    {
        if (null === $this->extension) {
            $this->extension = new MockServerExtension();
        }

        return $this->extension;
    }
}
