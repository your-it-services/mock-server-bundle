# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Fixed

## [1.0.1] - 2023-03-10

### Fixed

- Moved default http_client from config yaml to extension; preventing project conflicts.

## [1.0.0] - 2023-02-28

### Added

- Initial release.
- Supported formats:
  - JSON
  - JSON:API
  - SOAP
- Supported verbs: 
  - GET, POST, PATCH, DELETE (JSON)
  - GET, POST, PATCH, DELETE (JSON:API)
  - POST (SOAP)
